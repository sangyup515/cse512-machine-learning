import numpy as np
from random import shuffle
from sklearn import preprocessing
from sklearn import svm
from sklearn.metrics import classification_report

f1 = open("datasethw2/data1.txt", 'r')
f2 = open("datasethw2/data2.txt", 'r')
f3 = open("datasethw2/data3.txt", 'r')

dataset = list()
x_data = list()
y_data = list()


for row in f1:
    row = row.split('\t')
    dataset.append(row)
for row in f2:
    row = row.split('\t')
    dataset.append(row)
for row in f3:
    row = row.split('\t')
    dataset.append(row)
shuffle(dataset)
print("Dataset shuffled...")
for row in dataset:
    x_data.append([float(row[0]), float(row[1]), float(row[2]), float(row[3]), float(row[4])])
    y_data.append(int(row[5]))

n_features = len(x_data[0])
print("n_features :", n_features)
# Normalize train data 80%
x_data = x_data[:int(len(x_data)*0.8)]
x_data = preprocessing.normalize(x_data)
x_data = np.array(x_data).astype('float32')
y_data = y_data[:int(len(y_data)*0.8)]
y_data = np.array(y_data)
# Change y label into one-hot code
print("y_data:\n", y_data)

# Test data 20%
test = [[float(row[0]), float(row[1]), float(row[2]), float(row[3]), float(row[4])] for row in dataset[int(len(dataset)*0.8):]]
test = preprocessing.normalize(test)
test = np.array(test)
test_y = [int(row[5]) for row in dataset[int(len(dataset)*0.8):]]
test_y = np.array(test_y)


clf = svm.SVC(gamma=0.01, C=300)
clf.fit(x_data, y_data)
y_pred = clf.predict(test)

print("test_y:", test_y)
print("y_pred:", y_pred)

print(classification_report(test_y, y_pred))
