# Import MINST data
import input_data
import tensorflow as tf
import numpy as np
from random import shuffle
from sklearn import preprocessing
from sklearn.metrics import classification_report


f1 = open("datasethw2/data1.txt", 'r')
f2 = open("datasethw2/data2.txt", 'r')
f3 = open("datasethw2/data3.txt", 'r')

dataset = list()
x_data = list()
y_data = list()


for row in f1:
    row = row.split('\t')
    dataset.append(row)
for row in f2:
    row = row.split('\t')
    dataset.append(row)
for row in f3:
    row = row.split('\t')
    dataset.append(row)
shuffle(dataset)
print("Dataset shuffled...")
for row in dataset:
    x_data.append([float(row[0]), float(row[1]), float(row[2]), float(row[3]), float(row[4])])
    y_data.append(int(row[5])-1)

n_features = len(x_data[0])
print("n_features :", n_features)
# Normalize train data 80%
x_data = x_data[:int(len(x_data)*0.8)]
x_data = preprocessing.normalize(x_data)
x_data = np.array(x_data).astype('float32')
#y_data = np.reshape(y_data, (len(y_data), 1)).astype('float32')
y_data = y_data[:int(len(y_data)*0.8)]
y_data = np.array(y_data).astype('float32')
# Change y label into one-hot code
y_data = tf.one_hot(y_data, 3)
print("y_data:\n", y_data)

print("Dataset normalized...")

# Test data 20%
test = [[float(row[0]), float(row[1]), float(row[2]), float(row[3]), float(row[4])] for row in dataset[int(len(dataset)*0.8):]]
test = preprocessing.normalize(test)
test = np.array(test).astype('float32')
test_y = [int(row[5])-1 for row in dataset[int(len(dataset)*0.8):]]
test_y = np.array(test_y).astype('float32')
test_y = tf.one_hot(test_y, 3)

# Set parameters
learning_rate = 0.01
training_iteration = 301
display_step = 30

# TF graph input
x = tf.placeholder("float32", [None, n_features]) # mnist data image of shape 28*28=784
y = tf.placeholder("float32", [None, 3]) # 0-9 digits recognition => 10 classes

# Create a model

# Set model weights
# W1 = tf.Variable(tf.random_normal([5, 256]))
# b1 = tf.Variable(tf.zeros([1]))
#
# W2 = tf.Variable(tf.random_normal([256, 128]))
# b2 = tf.Variable(tf.zeros([1]))
#
# W3 = tf.Variable(tf.random_normal([128, 3]))
# b3 = tf.Variable(tf.zeros([1]))
W = tf.Variable(tf.random_normal([n_features, 3]))
b = tf.Variable(tf.zeros([1]))

# Construct a linear model
# L1 = tf.matmul(x, W1) + b1 # Softmax
# L1 = tf.nn.relu(L1)
# L2 = tf.matmul(L1, W2) + b2 # Softmax
# L2 = tf.nn.relu(L2)
# model = tf.nn.softmax(tf.matmul(L2, W3) + b3) # Softmax

model = tf.nn.softmax(tf.matmul(x, W) + b) # Softmax

# Minimize error using cross entropy
# Cross entropy
cost_function = -tf.reduce_sum(y*tf.log(model))
# Gradient Descent
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)

# Initializing the variables
init = tf.initialize_all_variables()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    # Training cycle
    for iteration in range(training_iteration):
        sess.run(optimizer, feed_dict={x: x_data, y: sess.run(y_data)})
        if iteration % display_step == 0:
            print("Iteration : ", iteration, "\nWeights : ", sess.run(W), "\nBias : ", sess.run(b))

    print("Tuning completed!")

    # Test the model
    predictions = tf.equal(tf.argmax(model, 1), tf.argmax(test_y, 1))

    # Calculate accuracy
    y_pred = sess.run(model, feed_dict={x: test, y: sess.run(test_y)})
    accuracy = tf.reduce_mean(tf.cast(predictions, "float32"))
    Y_test = np.argmax(sess.run(test_y), axis=1)
    # print("y_pred_prob:", y_pred)
    y_pred = np.argmax(y_pred, axis=1)
    print("y_pred:", y_pred)
    print("Y_test:", Y_test)
    print("for each class:::::")
    print(classification_report(Y_test, y_pred))

    print("Accuracy:", accuracy.eval({x: test, y: sess.run(test_y)}))

