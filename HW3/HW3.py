# Before running this code,
# make sure that HW3-CNN-dataset folder
# is in the same directory with this code.

from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.preprocessing import image
import glob
import numpy as np
from sklearn.utils import shuffle
from sklearn.metrics import classification_report

batch_size = 32
num_classes = 2
epochs = 20

# input image dimensions
img_rows, img_cols = 256, 256

# 32
# img_paths_man = glob.glob("HW3-CNN-dataset/womanman-32/man/*.png")
# img_paths_woman = glob.glob("HW3-CNN-dataset/womanman-32/woman/*.png")

# 256
img_paths_man = glob.glob("HW3-CNN-dataset/woman-man-256/woman-man-256/man/*.png")
img_paths_woman = glob.glob("HW3-CNN-dataset/woman-man-256/woman-man-256/woman/*.png")

img_arr_man = list()
img_arr_woman = list()

for imgs in img_paths_man:
    img = image.load_img(imgs, target_size=(img_rows, img_cols))
    img_arr_man.append(image.img_to_array(img))

for imgs in img_paths_woman:
    img = image.load_img(imgs, target_size=(img_rows, img_cols))
    img_arr_woman.append(image.img_to_array(img))

X = np.array(img_arr_man + img_arr_woman)
Y = [0 for x in range(len(img_arr_man))] + [1 for x in range(len(img_arr_woman))]

X, Y = shuffle(X, Y, random_state=0)

x_train = X[:int(len(X)*0.80)]
x_test = X[int(len(X)*0.80):]
y_train = Y[:int(len(X)*0.80)]
y_test = Y[int(len(X)*0.80):]

print(x_train.shape)

x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 3)
x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 3)
input_shape = (img_rows, img_cols, 3)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape))
model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))


model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.sgd(lr=0.005),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=2,
          validation_data=(x_test, y_test))
# score = model.evaluate(x_test, y_test, verbose=0)
# print('Test loss:', score[0])
# print('Test accuracy:', score[1])
Y_test = np.argmax(y_test, axis=1)
y_pred = model.predict_classes(x_test)
print("\n", classification_report(Y_test, y_pred))

